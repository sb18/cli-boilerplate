import fs from "fs";

import { handleFile } from "./handleFile";

/** get path to working directory and other cli arguments */
const [path, args] = process.argv.slice(2);

/** check if path exists */
if (!path || !fs.existsSync(path)) {
  console.error(`path "${path}" does not exist`);
  process.exit();
} else {
  process.chdir(path);
}

/** check if test flag is set */
const isTestRun: boolean = args?.includes("test");
if (isTestRun) {
  console.log("*** TEST RUN ***");
}

fs.readdirSync(".")
  .filter((filename) => filename.match(/\.btl$/i))
  .forEach(readFile);

function readFile(filename: string) {
  fs.readFile(filename, { encoding: "utf8" }, (error, data) =>
    error
      ? console.log(`couldn't read ${filename}`)
      : writeFile(filename, handleFile(filename, data))
  );
}

function writeFile(filename: string, data: string) {
  fs.writeFile(isTestRun ? `${filename}.out` : filename, data, (error) =>
    error
      ? console.log(`couldn't write ${filename}`)
      : console.log(`wrote ${filename}`)
  );
}
